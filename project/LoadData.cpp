#include "header/LoadData.h"

namespace LoadData {

	bool LoadMaterial(const char* objPath, Material& mat) {

		char aux[128];

		FILE * dotObj = fopen(objPath, "r");
		if (dotObj == NULL) {
			printf("Couldn't open .mtl file!\n");
			return false;
		}

		while (fscanf(dotObj, "%s", aux) != EOF) {
			if (strcmp(aux, "Ns") == 0) {
				fscanf(dotObj, "%f\n", &mat.Ns);
			}
			else if (strcmp(aux, "Ni") == 0) {
				fscanf(dotObj, "%f\n", &mat.Ni);
			}
			else if (strcmp(aux, "d") == 0) {
				fscanf(dotObj, "%f\n", &mat.d);
			}
			else if (strcmp(aux, "illum") == 0) {
				fscanf(dotObj, "%d\n", &mat.illum);
			}
			else if (strcmp(aux, "Ka") == 0) {
				glm::vec3 ka;
				fscanf(dotObj, "%f %f %f\n", &ka.x, &ka.y, &ka.z);
				mat.Ka = ka;
			}
			else if (strcmp(aux, "Kd") == 0) {
				glm::vec3 kd;
				fscanf(dotObj, "%f %f %f\n", &kd.x, &kd.y, &kd.z);
				mat.Kd = kd;
			}
			else if (strcmp(aux, "Ks") == 0) {
				glm::vec3 ks;
				fscanf(dotObj, "%f %f %f\n", &ks.x, &ks.y, &ks.z);
				mat.Ks = ks;
			}
			else if (strcmp(&aux[strlen(aux) - 4], ".tga") == 0) {
				strcpy(mat.map_Kd_filePath, "resources\\");
				strcat(mat.map_Kd_filePath, aux);
			}
		}
		fclose(dotObj);
		return true;
	}
	bool LoadModel(const char* objPath, Model& mod) {
		FILE * dotObj = fopen(objPath, "r");
		if (dotObj == NULL) {
			printf("Couldn't open .obj file!\n");
			return false;
		}

		char aux[128];
		mod.vertexCount = 0;
		mod.indexCount = 0;
		mod.textureCount = 0;
		mod.normalCount = 0;

		while (fscanf(dotObj, "%s", aux) != EOF) {

			if (strcmp(aux, "v") == 0) {
				mod.vertexCount++;
				glm::vec3 vertex;
				fscanf(dotObj, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
				mod.vertices.push_back(vertex);

			}
			else if (strcmp(aux, "vt") == 0) {
				mod.textureCount++;
				glm::vec2 uv;
				fscanf(dotObj, "%f %f\n", &uv.x, &uv.y);
				mod.uvs.push_back(uv);

			}
			else if (strcmp(aux, "vn") == 0) {
				mod.normalCount++;
				glm::vec3 normal;
				fscanf(dotObj, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
				mod.normals.push_back(normal);

			}
			else if (strcmp(aux, "f") == 0) {
				std::string vertex1, vertex2, vertex3;
				unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
				fscanf(dotObj, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);

				mod.v_index.push_back(vertexIndex[0]);
				mod.v_index.push_back(vertexIndex[1]);
				mod.v_index.push_back(vertexIndex[2]);
				mod.uv_index.push_back(uvIndex[0]);
				mod.uv_index.push_back(uvIndex[1]);
				mod.uv_index.push_back(uvIndex[2]);
				mod.n_index.push_back(normalIndex[0]);
				mod.n_index.push_back(normalIndex[1]);
				mod.n_index.push_back(normalIndex[2]);
				mod.indexCount += 3;

			}
			else if (strcmp(&aux[strlen(aux) - 4], ".mtl") == 0) {
				strcpy(mod.material_filePath, "resources\\");
				strcat(mod.material_filePath, aux);
			}
		}

		fclose(dotObj);
		return true;
	}
	Model::~Model()
	{
		vertices.clear();
		uvs.clear();
		normals.clear();
		v_index.clear();
		uv_index.clear();
		n_index.clear();
	}
}