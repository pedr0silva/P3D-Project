#version 440 core

layout(location = 1) uniform mat4 View;


layout (location = 0) in vec2 textureCoord;
layout (location = 1) in vec3 normalCoords;
layout (location = 2) in vec3 positionEyeSpace;

layout(binding = 0, location = 5) uniform sampler2D textureMap;

layout (location = 0) out vec4 fColor;


struct Material{
	vec3 emissive;
	vec3 ambient;		// Ka
	vec3 diffuse;		// kd
	vec3 specular;		// ke
	float shininess;
};

layout(location = 6) uniform Material material;

// Estrutura da fonte de luz ambiente global
struct AmbientLight {
	vec3 ambient;	
};

layout(location = 11) uniform AmbientLight ambientLight; // Fonte de luz ambiente global


// Estrutura de uma fonte de luz pontual
struct PointLight	{
	vec3 position;		// Posi��o do ponto de luz, espa�o do mundo
	
	vec3 ambient;		// Componente de luz ambiente
	vec3 diffuse;		// Componente de luz difusa
	vec3 specular;		// Componente de luz especular
	
	float constant;		// Coeficiente de atenua��o constante
	float linear;		// Coeficiente de atenua��o linear
	float quadratic;	// Coeficiente de atenua��o quadr�tica
};
layout(location = 30) uniform PointLight pointLight;


// Estrutura de uma fonte de luz direcional
struct DirectionalLight	{
	vec3 direction;		// Dire��o da luz, espa�o do mundo
	
	vec3 ambient;		// Componente de luz ambiente
	vec3 diffuse;		// Componente de luz difusa
	vec3 specular;		// Componente de luz especular
};

layout(location = 40) uniform DirectionalLight directionalLight; // Fonte de luz direcional


// Estrutura de uma fonte de luz conica
struct SpotLight {

	vec3 direction;		// Dire��o da luz, espa�o do mundo
	vec3 position;		// Posi��o do ponto de luz, espa�o do mundo	
	float cutOff;
	float outerCutOff;

	vec3 ambient;		// Componente de luz ambiente
	vec3 diffuse;		// Componente de luz difusa
	vec3 specular;		// Componente de luz especular

	float constant;
	float linear;
	float quadratic;
};
layout(location = 50) uniform SpotLight spotLight;

//Assinaturas
vec4 calcAmbientLight(AmbientLight light);
vec4 calcDirectionalLight(DirectionalLight light);
vec4 calcPointLight(PointLight light);
vec4 calcSpotLight(SpotLight light);

vec4 light[4];
void main()
{	
	
	light[0] = calcAmbientLight(ambientLight);
	light[1] = calcDirectionalLight(directionalLight);
    light[2] = calcPointLight(pointLight);
	light[3] = calcSpotLight(spotLight);

	fColor = (light[0] + light[1] + light[2] + light[3]) * texture(textureMap, textureCoord);
}

vec4 calcAmbientLight(AmbientLight light) {
	vec4 ambient = vec4(material.ambient * light.ambient, 1.0);
	return ambient;
}


vec4 calcDirectionalLight(DirectionalLight light) {
	vec4 ambient = vec4(material.ambient * light.ambient, 1.0);

	vec3 lightDirectionEyeSpace = (View * vec4(light.direction, 0.0)).xyz;
	vec3 L = normalize(-lightDirectionEyeSpace); // Dire��o inversa � da dire��o luz.
	vec3 N = normalize(normalCoords);
	float NdotL = max(dot(N, L), 0.0);
	vec4 diffuse = vec4(material.diffuse * light.diffuse, 1.0) * NdotL;
	

	vec3 V = normalize(-positionEyeSpace);
	vec3 R = reflect(-L, N);
	float RdotV = max(dot(R, V), 0.0);
	vec4 specular = pow(RdotV, material.shininess) * vec4(light.specular * material.specular, 1.0);

	return (ambient + diffuse + specular);
}


vec4 calcPointLight(PointLight light) {
	vec4 ambient = vec4(material.ambient * light.ambient, 1.0);

	vec3 lightPositionEyeSpace = (View * vec4(light.position, 1.0)).xyz;
	vec3 L = normalize(lightPositionEyeSpace - positionEyeSpace);
	vec3 N = normalize(normalCoords);
	float NdotL = max(dot(N, L), 0.0);
	vec4 diffuse = vec4(material.diffuse * light.diffuse, 1.0) * NdotL;


	vec3 V = normalize(-positionEyeSpace);
	vec3 R = reflect(-L, N);
	float RdotV = max(dot(R, V), 0.0);
	vec4 specular = pow(RdotV, material.shininess) * vec4(light.specular * material.specular, 1.0);
	

	// attenuation
	float dist = length(mat3(View) * light.position - positionEyeSpace);
	float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * (dist * dist));
	
	return vec4(attenuation * (ambient + diffuse + specular));
}



vec4 calcSpotLight(SpotLight light){
	vec4 ambient = vec4(material.ambient * light.ambient, 1.0);

	vec3 lightPositionEyeSpace = (View * vec4(light.position, 1.0)).xyz;
	vec3 L = normalize(lightPositionEyeSpace - positionEyeSpace);
	vec3 N = normalize(normalCoords);
	float NdotL = max(dot(N, L), 0.0);
	vec4 diffuse = vec4(material.diffuse * light.diffuse, 1.0) * NdotL;

	vec3 V = normalize(-positionEyeSpace);
	vec3 R = reflect(-L, N);
	float RdotV = max(dot(R, V), 0.0);
	vec4 specular = pow(RdotV, material.shininess) * vec4(light.specular * material.specular, 1.0);

	float theta = dot(lightPositionEyeSpace, normalize(-light.direction));
    float epsilon = (light.cutOff - light.outerCutOff);
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    diffuse  *= intensity;
    specular *= intensity;

    // attenuation
	float dist = length(mat3(View) * light.position - positionEyeSpace);
	float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * (dist * dist));

	return attenuation * (ambient + diffuse + specular);
}

