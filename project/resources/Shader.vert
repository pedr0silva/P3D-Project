#version 440 core

layout(location = 0) uniform mat4 Model;
layout(location = 1) uniform mat4 View;
layout(location = 2) uniform mat4 ModelView;
layout(location = 3) uniform mat4 Projection;
layout(location = 4) uniform mat3 NormalMatrix;

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec2 vTextureCoords;
layout(location = 2) in vec3 vNormalCoords;

layout(location = 0) out vec2 textureVector;
layout(location = 1) out vec3 normalCoordsOut;
layout(location = 2) out vec3 positionEyeSpace;
void main()
{
	//positionEyeSpace = vec3(ModelView * vec4(vPosition, 1.0f));
	//normalCoordsOut = normalize(NormalMatrix * vNormalCoords);
	positionEyeSpace = vPosition;
	gl_Position = Projection * ModelView * vec4(vPosition, 1.0f);
	textureVector = vTextureCoords;
	normalCoordsOut = vNormalCoords;
}