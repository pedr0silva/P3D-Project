#pragma once

#define GLEW_STATIC
#include <GL/glew.h>

namespace ShaderLoader {
	struct Shader {
		GLenum       type;
		const char*  filename;
		GLuint       shader;
	};
	bool Load(const char* objPath, Shader& shader);
}