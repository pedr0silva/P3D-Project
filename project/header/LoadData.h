#pragma once
#pragma warning(disable : 4996)

#include <iostream>
#include <vector>

#define GLEW_STATIC
#include <GL/glew.h>
#define GLFW_USE_DWM_SWAP_INTERVAL
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>

/* **************************************************************************
								LOAD DATA
	Purpose:
	- Contains structs that will hold data in CPU. These are raw data read from
	the source files.

								 MATERIAL
	- Stores data relative to the material's characteristics.
		-Ns --- Specular Exponent.
		-Ka --- Ambiente Reflectivity.
		-Kd --- Diffuse Reflectivity.
		-Ks --- Specular Reflectivity.
		-Ni --- Optical Density.
		-d --- Dissolve.
		-illum --- Illumination Model.
		-filePath --- Path to image file.

								   MODEL
	-Stores data relative to a model's geometry, it's vertexes, uv coordinates, 
	and normals. Also stores indexes, some auxiliary counters, and the file path
	to the material.
   ************************************************************************** */


namespace LoadData {

	typedef struct Material {

		float Ns;
		glm::vec3 Ka;
		glm::vec3 Kd;
		glm::vec3 Ks;
		float Ni;
		float d;
		int illum;
		char map_Kd_filePath[128];
	} Material;

	bool LoadMaterial(const char* objPath, Material& mat);

	typedef struct Model {
		GLfloat vertexCount;
		GLfloat indexCount;
		GLfloat textureCount;
		GLfloat normalCount;
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> uvs;
		std::vector<glm::vec3> normals;
		std::vector<unsigned int> v_index;
		std::vector<unsigned int> uv_index;
		std::vector<unsigned int> n_index;
		char material_filePath[128];
		~Model();
	} Model;

	bool LoadModel(const char* objPath, Model& mod);
}