#pragma once
#define GLEW_STATIC
#include <GL/glew.h>
#define GLFW_USE_DWM_SWAP_INTERVAL

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm\gtc\matrix_inverse.hpp>

#include "LoadData.h"

/* **************************************************************************
								GAME DATA
	Purpose:
	- Create classes that store data in a organized way, ready to be used
	in the rendering process.

							  RENDER OBJECT
	- Stores all data relative to the drawable object, in other words the
	vertex array object and it's buffer objects.
	- Capable of rendering itself, and to change the local model matrix.

								 CAMERA
	- Stores camera position, and all the camera-related matrixs (View and Projection).
	- Capable of changing it's matrixs with the movement of the camera.

							   GAME STATE
	- Stores important generic data for the game to run.
   ************************************************************************** */

namespace GameData {
	struct GameState;

	class RenderObject {
	private:
		GLsizei indexCount;
	public:
		RenderObject(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);
		~RenderObject();
		void LoadData(LoadData::Model model, LoadData::Material mat, unsigned char *texture, int textureWidht, int textureHeight, int textureChannels);
		void LinkToShader(GLuint program);
		void Rotate(glm::vec3 angles);
		void Draw(GameState *state);

		glm::vec3 position;
		glm::vec3 rotation;
		glm::vec3 scale;
		glm::mat4 modelMatrix;

		GLuint ModelVao;
		GLuint VertexBuffer;
		GLuint UvBuffer;
		GLuint NormalBuffer;

		GLuint TextureUnit;
	};

	class Camera {
	private:
		glm::mat4 rotationMatrix;
	public:
		Camera(glm::vec3 position, glm::vec3 rotation, float width, float height);
		void Move(glm::vec3 deltaPosition);
		glm::vec3 position;
		glm::vec3 forward;
		glm::vec3 left;
		glm::vec3 up;
		glm::vec3 rotation;
		glm::mat4 viewMatrix;
		glm::mat4 projectionMatrix;

	};

	struct GameState {
		GLFWwindow *window;
		GLuint program;
		Camera *camera;
		glm::vec2 mousePosition;
		double lastFrameTime;
		double deltaTime;
	};
}