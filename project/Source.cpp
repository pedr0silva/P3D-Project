﻿#pragma comment(lib, "glew32s.lib")
#pragma comment(lib, "glfw3.lib")
#pragma comment(lib, "opengl32.lib")

#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>
#define GLFW_USE_DWM_SWAP_INTERVAL
#include <GLFW/glfw3.h>

#include "header/LoadData.h"
#include "header/GameData.h"
#include "header/LoadShaders.h"

#define STB_IMAGE_IMPLEMENTATION
#include "header/stb_image.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

void init(void);
void update(void);
void display(void);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void mouseCallback(GLFWwindow* window, int button, int action, int mods);
void cursorCallback(GLFWwindow* window, double xpos, double ypos);

#define WIDTH 800
#define HEIGHT 600

//Model Data
GameData::RenderObject *IronMan;
GameData::GameState state;

bool ambientLight;
bool directLight;
bool ponctuaLight;
bool spotLight;
bool mouseClick;

glm::vec3 pointPosition;

void print_error(int error, const char *description) {
	std::cout << description << std::endl;
}

int main(void) {

	glfwSetErrorCallback(print_error);

	if (!glfwInit()) return -1;

	state.window = glfwCreateWindow(WIDTH, HEIGHT, "P3D", NULL, NULL);
	if (state.window == NULL) {
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(state.window);

	glewExperimental = GL_TRUE;
	glewInit();

	init();

	glfwSetInputMode(state.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetKeyCallback(state.window, keyCallback);
	glfwSetScrollCallback(state.window, scrollCallback);
	glfwSetMouseButtonCallback(state.window, mouseCallback);
	glfwSetCursorPosCallback(state.window, cursorCallback);

	while (!glfwWindowShouldClose(state.window)) {
		double currentFrame = glfwGetTime();
		state.deltaTime = currentFrame - state.lastFrameTime;
		state.lastFrameTime = currentFrame;

		update();

		display();

		glfwSwapBuffers(state.window);
		glfwPollEvents();
	}

	delete IronMan;

	glfwTerminate();
	return 0;
}

void init(void) {

	IronMan = new GameData::RenderObject(glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(1, 1, 1));
	state.camera = new GameData::Camera(glm::vec3(0, 2, 5), glm::vec3(), WIDTH, HEIGHT);

	//----------Data load to RAM----------
	LoadData::Model imModel;
	LoadData::LoadModel("resources\\Iron_Man.obj", imModel);

	LoadData::Material imMaterial;
	LoadData::LoadMaterial(imModel.material_filePath, imMaterial);

	stbi_set_flip_vertically_on_load(true);
	int w, h, c;
	unsigned char *image = (unsigned char *)stbi_load(imMaterial.map_Kd_filePath, &w, &h, &c, 0);

	//----------Data transfer to VRAM----------
	IronMan->LoadData(imModel, imMaterial, image, w, h, c);
	stbi_image_free(image);

	//----------Load Shaders----------
	ShaderInfo shaders[] = {
		{ GL_VERTEX_SHADER,   "resources\\Shader.vert" },
		{ GL_FRAGMENT_SHADER, "resources\\Shader.frag" },
		{ GL_NONE, NULL }
	};


	state.program = LoadShaders(shaders);
	if (!state.program) exit(EXIT_FAILURE);
	glUseProgram(state.program);

	//----------Link Data to Shaders----------
	IronMan->LinkToShader(state.program);

	GLint modelId = glGetProgramResourceLocation(state.program, GL_UNIFORM, "Model");
	glProgramUniformMatrix4fv(state.program, modelId, 1, GL_FALSE, glm::value_ptr(glm::mat4()));
	GLint viewId = glGetProgramResourceLocation(state.program, GL_UNIFORM, "View");
	glProgramUniformMatrix4fv(state.program, viewId, 1, GL_FALSE, glm::value_ptr(state.camera->viewMatrix));
	GLint modelviewId = glGetProgramResourceLocation(state.program, GL_UNIFORM, "ModelView");
	glProgramUniformMatrix4fv(state.program, modelviewId, 1, GL_FALSE, glm::value_ptr(glm::mat4()));
	GLint projectionId = glGetProgramResourceLocation(state.program, GL_UNIFORM, "Projection");
	glProgramUniformMatrix4fv(state.program, projectionId, 1, GL_FALSE, glm::value_ptr(state.camera->projectionMatrix));
	GLint normalMatrixId = glGetProgramResourceLocation(state.program, GL_UNIFORM, "NormalMatrix");
	glProgramUniformMatrix3fv(state.program, normalMatrixId, 1, GL_FALSE, glm::value_ptr(glm::mat4()));
	//----------------Light--------------------//
			//Model
			//uniforms


			//MATERIAL
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "material.emissive"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "material.ambient"), 1, glm::value_ptr(imMaterial.Ka));
	//glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "material.ambient"), 1, glm::value_ptr(glm::vec3(1.0, 1.0, 1.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "material.diffuse"), 1, glm::value_ptr(imMaterial.Kd));
	//glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "material.diffuse"), 1, glm::value_ptr(glm::vec3(1.0, 1.0, 1.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "material.specular"), 1, glm::value_ptr(imMaterial.Ks));
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "material.shininess"), imMaterial.Ns);

	// Fonte de luz ambiente global
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "ambientLight.ambient"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));

	// Fonte de luz direcional
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "directionalLight.direction"), 1, glm::value_ptr(glm::vec3(1.0, -1.0, 0.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "directionalLight.ambient"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "directionalLight.diffuse"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "directionalLight.specular"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));

	// Fonte de luz pontual
	pointPosition = glm::vec3(0.0, 1.0, -1.0);
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.position"), 1, glm::value_ptr(pointPosition));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.ambient"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.diffuse"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.specular"), 1, glm::value_ptr(glm::vec3(0.0, 0.0, 0.0)));
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.constant"), 1.0f);
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.linear"), 0.06f);
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.quadratic"), 0.02f);


	// Fonte de luz Conica
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.position"), 1, glm::value_ptr(glm::vec3(0.0, 2.0, -1.0)));
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.cutOff"), 0.8f);
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.outerCutOff"), 0.8f);
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.ambient"), 1, glm::value_ptr(glm::vec3(1.0, 1.0, 1.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLightt.diffuse"), 1, glm::value_ptr(glm::vec3(1.0, 0.0, 1.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.specular"), 1, glm::value_ptr(glm::vec3(1.0, 1.0, 1.0)));
	glProgramUniform3fv(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.direction"), 1, glm::value_ptr(glm::vec3(0.0, -1.0, 0.0)));
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.constant"), 1.0f);
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.linear"), 0.06f);
	glProgramUniform1f(state.program, glGetProgramResourceLocation(state.program, GL_UNIFORM, "spotLight.quadratic"), 0.02f);




	//----------Viewport----------
	glViewport(0, 0, WIDTH, HEIGHT);

	//----------Final Parameters----------
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
}

void update(void) {
	GLint viewId = glGetProgramResourceLocation(state.program, GL_UNIFORM, "View");
	glUniformMatrix4fv(viewId, 1, GL_FALSE, glm::value_ptr(state.camera->viewMatrix));
	GLint projectionId = glGetProgramResourceLocation(state.program, GL_UNIFORM, "Projection");
	glUniformMatrix4fv(viewId, 1, GL_FALSE, glm::value_ptr(state.camera->projectionMatrix));
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);

	IronMan->Draw(&state);
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	int AmbientLightB = glfwGetKey(window, GLFW_KEY_1);
	int DirectionaltLightB = glfwGetKey(window, GLFW_KEY_2);
	int PonctualLightB = glfwGetKey(window, GLFW_KEY_3);
	int SpotLightB = glfwGetKey(window, GLFW_KEY_4);

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	 if (key == GLFW_KEY_LEFT) {
		pointPosition.x -= 100 * state.deltaTime;
		glUniform3fv(glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.position"), 1, glm::value_ptr(pointPosition));
	}
	else if (key == GLFW_KEY_RIGHT) {
		pointPosition.x += 100 * state.deltaTime;
		glUniform3fv(glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.position"), 1, glm::value_ptr(pointPosition));
	}
	if (key == GLFW_KEY_UP) {
		pointPosition.y += 100 * state.deltaTime;
		glUniform3fv(glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.position"), 1, glm::value_ptr(pointPosition));
	}
	else if (key == GLFW_KEY_DOWN ) {
		pointPosition.y -= 100 * state.deltaTime;
		glUniform3fv(glGetProgramResourceLocation(state.program, GL_UNIFORM, "pointLight.position"), 1, glm::value_ptr(pointPosition));
	}



	/*‘1’ – Ativar / desativar fonte de luz ambiente;
	o ‘2’ – Ativar / desativar fonte de luz direcional;
	o ‘3’ – Ativar / desativar fonte de luz pontual;
	o ‘4’ – Ativar / desativar fonte de luz cónica.
	*/

	if (AmbientLightB == GLFW_PRESS) {
		GLint ambientLightU = glGetUniformLocation(state.program, "ambientLight.ambient");
		switch (ambientLight)
		{
			case	true:
			{
				ambientLight = false;
				glUniform3f(ambientLightU, 0.0, 0.0, 0.0);
				break;
			}

			case	false:
			{
				ambientLight = true;
				glUniform3f(ambientLightU, 1.0, 1.0, 1.0);
				break;
			}
		}

	}

	else if (DirectionaltLightB == GLFW_PRESS) {
		GLint directLightUA = glGetUniformLocation(state.program, "directionalLight.ambient");
		GLint directLightUD = glGetUniformLocation(state.program, "directionalLight.diffuse");
		GLint directLightUS = glGetUniformLocation(state.program, "directionalLight.specular");

		switch (directLight)
		{
			case	true: {
				directLight = false; 
				glUniform3f(directLightUA, 0.0, 0.0, 0.0);
				glUniform3f(directLightUD, 0.0, 0.0, 0.0);
				glUniform3f(directLightUS, 0.0, 0.0, 0.0);

				break; 
			}

			case	false: {
				directLight = true;
				glUniform3f(directLightUA, 1.0, 1.0, 1.0);
				glUniform3f(directLightUD, 2.0, 2.0, 2.0);
				glUniform3f(directLightUS, 1.0, 1.0, 1.0);
				break; 
			}

		}

	}

	else if (PonctualLightB == GLFW_PRESS) {
		GLint ponctualLightUA = glGetUniformLocation(state.program, "pointLight.ambient");
		GLint ponctualLightUD = glGetUniformLocation(state.program, "pointLight.diffuse");
		GLint ponctualLightUS = glGetUniformLocation(state.program, "pointLight.specular");

		switch (ponctuaLight)
		{
			case	true: { 
				ponctuaLight = false;
				glUniform3f(ponctualLightUA, 0.0, 0.0, 0.0);
				glUniform3f(ponctualLightUD, 0.0, 0.0, 0.0);
				glUniform3f(ponctualLightUS, 0.0, 0.0, 0.0);
				break; 
			}

			case	false: {
				ponctuaLight = true; 
				glUniform3f(ponctualLightUA, 1.0, 1.0, 1.0);
				glUniform3f(ponctualLightUD, 2.0, 2.0, 2.0);
				glUniform3f(ponctualLightUS, 1.0, 1.0, 1.0);
				break; 
			}
		}
	}

	else if (SpotLightB == GLFW_PRESS) {
		GLint spotLightUA = glGetUniformLocation(state.program, "spotLight.ambient");
		GLint spotLightUD = glGetUniformLocation(state.program, "spotLight.diffuse");
		GLint spotLightUS = glGetUniformLocation(state.program, "spotLight.specular");

		switch (spotLight)
		{
			case	true: {
				spotLight = false;
				glUniform3f(spotLightUA, 0.0, 0.0, 0.0);
				glUniform3f(spotLightUD, 0.0, 0.0, 0.0);
				glUniform3f(spotLightUS, 0.0, 0.0, 0.0);
				break; 
			}

			case	false: {
				spotLight = true; 
				glUniform3f(spotLightUA, 1.0, 1.0, 1.0);
				glUniform3f(spotLightUD, 10.0, 10.0, 10.0);
				glUniform3f(spotLightUS, 1.0, 1.0, 1.0);
				break; 
			}
		}
	}
}
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
	if (yoffset > 0) {
		state.camera->Move(state.camera->forward);
	}
	else {
		state.camera->Move(-(state.camera->forward));
	}
}
void mouseCallback(GLFWwindow* window, int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_LEFT) {
		if (GLFW_PRESS == action)
			mouseClick = true;
		else if (GLFW_RELEASE == action)
			mouseClick = false;
	}
}
void cursorCallback(GLFWwindow* window, double xpos, double ypos) {
	if (mouseClick) {
		if (xpos != 0) {
			IronMan->Rotate(glm::vec3(0, (xpos - state.mousePosition.x) * state.deltaTime, 0));
		}
	}
	state.mousePosition.x = xpos;
	state.mousePosition.y = ypos;
}