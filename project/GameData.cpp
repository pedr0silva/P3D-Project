﻿#include "header\\GameData.h"

using namespace GameData;

RenderObject::RenderObject(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale) : position(position), rotation(rotation), scale(scale)
{
	modelMatrix = glm::mat4();
	modelMatrix = glm::scale(modelMatrix, scale);
	modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0, 1, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0, 0, 1));
	modelMatrix = glm::translate(modelMatrix, position);
}

RenderObject::~RenderObject()
{
	glDeleteBuffers(1, &(RenderObject::VertexBuffer));
	glDeleteBuffers(1, &(RenderObject::UvBuffer));
	glDeleteBuffers(1, &(RenderObject::NormalBuffer));

	glDeleteVertexArrays(1, &(RenderObject::ModelVao));

	glDeleteTextures(1, &(RenderObject::TextureUnit));
}

void RenderObject::LoadData(LoadData::Model model, LoadData::Material mat, unsigned char *texture, int textureWidht, int textureHeight, int textureChannels) {
	RenderObject::indexCount = model.indexCount;

	//----------------------MODEL VAO----------------------//
	glGenVertexArrays(1, &(RenderObject::ModelVao));
	glBindVertexArray(RenderObject::ModelVao);

	//----------------------PROCESS DATA WITH INDEX----------------------//
	glm::vec3 *vec_aux = new glm::vec3[RenderObject::indexCount];
	for (int i = 0; i < RenderObject::indexCount; i++)
	{
		vec_aux[i] = model.vertices.at(model.v_index.at(i) - 1);
	}

	glm::vec2 *uv_aux = new glm::vec2[RenderObject::indexCount];
	for (int i = 0; i < RenderObject::indexCount; i++) 
	{
		uv_aux[i] = model.uvs.at(model.uv_index.at(i) - 1);
	}

	glm::vec3 *normal_aux = new glm::vec3[RenderObject::indexCount];
	for (int i = 0; i < RenderObject::indexCount; i++) {
		normal_aux[i] = model.normals.at(model.n_index.at(i) - 1);
	}

	//----------------------VERTEX----------------------//
	glGenBuffers(1, &(RenderObject::VertexBuffer));
	glBindBuffer(GL_ARRAY_BUFFER, RenderObject::VertexBuffer);
	glBufferStorage(GL_ARRAY_BUFFER, RenderObject::indexCount * sizeof(glm::vec3), glm::value_ptr(vec_aux[0]), 0);

	//----------------------UV----------------------//
	glGenBuffers(1, &(RenderObject::UvBuffer));
	glBindBuffer(GL_ARRAY_BUFFER, RenderObject::UvBuffer);
	glBufferStorage(GL_ARRAY_BUFFER, RenderObject::indexCount * sizeof(glm::vec2), glm::value_ptr(uv_aux[0]), 0);

	//----------------------UV----------------------//
	glGenBuffers(1, &(RenderObject::NormalBuffer));
	glBindBuffer(GL_ARRAY_BUFFER, RenderObject::NormalBuffer);
	glBufferStorage(GL_ARRAY_BUFFER, RenderObject::indexCount * sizeof(glm::vec3), glm::value_ptr(normal_aux[0]), 0);

	delete[] vec_aux;
	delete[] uv_aux;
	delete[] normal_aux;

	//----------------------TEXTURE----------------------//
	glGenTextures(1, &(RenderObject::TextureUnit));
	glBindTexture(GL_TEXTURE_2D, RenderObject::TextureUnit);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidht, textureHeight, 0, textureChannels == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, texture);
	glGenerateMipmap(GL_TEXTURE_2D);
}

void RenderObject::LinkToShader(GLuint program) {
	GLint coordsID = glGetProgramResourceLocation(program, GL_PROGRAM_INPUT, "vPosition");
	glBindBuffer(GL_ARRAY_BUFFER, RenderObject::VertexBuffer);
	glVertexAttribPointer(coordsID, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);
	glEnableVertexAttribArray(coordsID);

	GLint textureID = glGetProgramResourceLocation(program, GL_PROGRAM_INPUT, "vTextureCoords");
	glBindBuffer(GL_ARRAY_BUFFER, RenderObject::UvBuffer);
	glVertexAttribPointer(textureID, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), (void*)0);
	glEnableVertexAttribArray(textureID);

	GLint normalID = glGetProgramResourceLocation(program, GL_PROGRAM_INPUT, "vNormalCoords");
	glBindBuffer(GL_ARRAY_BUFFER, RenderObject::NormalBuffer);
	glVertexAttribPointer(normalID, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);
	glEnableVertexAttribArray(normalID);

	GLint textureMapID = glGetProgramResourceLocation(program, GL_UNIFORM, "textureMap");
	glProgramUniform1i(program, textureMapID, 0);
}

void RenderObject::Rotate(glm::vec3 angles)
{
	RenderObject::rotation += angles;
	modelMatrix = glm::mat4();
	modelMatrix = glm::scale(modelMatrix, scale);
	modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0, 1, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0, 0, 1));
	modelMatrix = glm::translate(modelMatrix, position);
}

void RenderObject::Draw(GameState *state)
{
	glBindVertexArray(RenderObject::ModelVao);
	GLint modelId = glGetProgramResourceLocation(state->program, GL_UNIFORM, "Model");
	glUniformMatrix4fv(modelId, 1, GL_FALSE, glm::value_ptr(RenderObject::modelMatrix));

	glm::mat4 modelView = (state->camera->viewMatrix) * RenderObject::modelMatrix;
	GLint modelViewId = glGetProgramResourceLocation(state->program, GL_UNIFORM, "ModelView");
	glUniformMatrix4fv(modelViewId, 1, GL_FALSE, glm::value_ptr(modelView));

	glm::mat4 normalMatrix = glm::inverseTranspose(glm::mat3(modelView));
	GLint normalMatrixId = glGetProgramResourceLocation(state->program, GL_UNIFORM, "NormalMatrix");
	glUniformMatrix3fv(normalMatrixId, 1, GL_FALSE, glm::value_ptr(normalMatrix));

	glDrawArrays(GL_TRIANGLES, 0, RenderObject::indexCount);
}

Camera::Camera(glm::vec3 position, glm::vec3 rotation, float width, float height) : position(position), rotation(rotation)
{
	rotationMatrix = glm::mat4();
	rotationMatrix = glm::rotate(rotationMatrix, rotation.x, glm::vec3(1, 0, 0));
	rotationMatrix = glm::rotate(rotationMatrix, rotation.y, glm::vec3(0, 1, 0));
	rotationMatrix = glm::rotate(rotationMatrix, rotation.z, glm::vec3(0, 0, 1));

	forward = rotationMatrix * glm::vec4(0.0f, 0.0f, -1.0f, 0);
	left = rotationMatrix * glm::vec4(-1.0f, 0.0f, 0.0f, 0);
	up = rotationMatrix * glm::vec4(0.0f, 1.0f, 0.0f, 0);

	viewMatrix = glm::lookAt(position, position + forward, glm::vec3(0.0f, 1.0f, 0.0f));
	projectionMatrix = glm::perspective(glm::radians(45.0f), width / height, 0.1f, 100.0f);
}

void Camera::Move(glm::vec3 deltaPosition)
{
	Camera::position += deltaPosition;
	viewMatrix = glm::lookAt(position, position + forward, glm::vec3(0.0f, 1.0f, 0.0f));

}

